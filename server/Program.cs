﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IronButane;

namespace server {
    internal class Program {
        private static ButaneUDP _server;
        private static void Main(string[] args) {
            _server = new ButaneUDP(1133, Receive);
            Console.ReadLine();
        }

        private static void Receive(byte[] msg, string ip, int port) {
            Console.WriteLine("IP = {0}; port = {1}", ip, port);
            var str = Encoding.ASCII.GetString(msg, 0, 255);
            _server.Send(ip, port, Encoding.ASCII.GetBytes(str));
            Console.WriteLine(str);
        }
    }
}