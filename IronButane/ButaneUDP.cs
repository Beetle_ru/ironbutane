﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace IronButane {
    public delegate void ReceiveCallbackDelegate(byte[] msg, string ip, int port);
    public class ButaneUDP {
        public const int MSGLEN = 65507;
        private readonly byte[] _buffer;
        private readonly ReceiveCallbackDelegate _receiveCallback;
        private readonly Thread _receiver;
        private readonly Socket _sock;

        public ButaneUDP(int port, ReceiveCallbackDelegate receiveCallback) {
            _sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            var iep = new IPEndPoint(IPAddress.Any, port);
            var ep = (EndPoint) iep;
            _sock.Bind(ep);
            _receiveCallback = receiveCallback;

            _buffer = new byte[MSGLEN];

            _receiver = new Thread(ListenProcess);
            _receiver.IsBackground = true;
            _receiver.Start();
        }

        public void Send(string ip, int port, byte[] msg) {
            IPAddress broadcast = IPAddress.Parse(ip);
            var ep = new IPEndPoint(broadcast, port);
            _sock.SendTo(msg, ep);
        }

        private void ListenProcess(Object sender) {
            var iep = new IPEndPoint(IPAddress.Any, 0);
            var ep = (EndPoint) iep;

            while (true)
                try {
                    int recBytes = _sock.ReceiveFrom(_buffer, ref ep);
                    _receiveCallback(_buffer, ((IPEndPoint) ep).Address.ToString(), ((IPEndPoint) ep).Port);
                    CleanBuffer(recBytes);
                }
                catch (Exception e) {
                    Thread.Sleep(100);
                    Console.WriteLine(e.Message);
                }
        }

        private void CleanBuffer(int recBytes) {
            for (int i = 0; (i <= recBytes) && (i < MSGLEN); i++) _buffer[i] = 0;
        }
    }
}