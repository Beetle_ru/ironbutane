﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IronButane;

namespace client {
    internal class Program {
        private static void Main(string[] args) {
            var client = new ButaneUDP(0, Receive);
            const string ip = "127.0.0.1";
            const int port = 1133;

            var msgl = new MessageLite("Test.Integer", 1133);
            client.Send(ip, port, msgl.Serialize());

            msgl = new MessageLite("Test.Double", 11.33);
            client.Send(ip, port, msgl.Serialize());

            msgl = new MessageLite("Test.String", "message: !@#$%^&*(){}\nqwertyuiop[]asdfghjkl;'zxcvbnm,./\nйцукенгшщзхъфывапролджэячсмитьбю.");
            client.Send(ip, port, msgl.Serialize());

            Console.ReadLine();
        }

        private static void Receive(byte[] msg, string ip, int port) {
            Console.WriteLine("IP = {0}; port = {1}", ip, port);
            var msgl = new MessageLite(msg);
            Console.WriteLine("{0} -> {1}",msgl.Key, msgl.Value); 
        }
    }
}